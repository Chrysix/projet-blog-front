import { Row, Col } from 'antd';
import { Menu } from "antd";
import { Header } from 'antd/lib/layout/layout';
import { useDispatch, useSelector } from "react-redux";
import { Link, useLocation } from "react-router-dom";
import { logout } from "../stores/authSlice";

export function Nav() {

    const location = useLocation();

    const dispatch = useDispatch();
    const user = useSelector(state => state.auth.user);

    
    // const loginShow = useSelector(state=> state.auth.user)

    return (

        <Menu className='nav' mode="horizontal" selectedKeys={[location.pathname]}>

            <Row>
                <Col span={24}>
                    <Menu.Item key="/">
                        <Link to="/">Home</Link>
                    </Menu.Item>
                </Col>
                <Col span={6}>
                    {/*<Search className='search' placeholder="Search User" onSearch={onSearch} style={{ width: 900 }} />*/}
                </Col>
            </Row>
            


            <Row>
                <Header className='nav-flex'>
                    <Col span={12} >
                        {user ? <Menu.Item key="/personal">
                            <Link to="/personal">Personal</Link>
                        </Menu.Item>
                            : false}

                        {user ? true
                            :
                            <Menu.Item key="/register">
                                <Link to="/register">Register</Link>
                            </Menu.Item>}
                    </Col>
                    <Col span={24}>
                        {user ? <Menu.Item key="/logout" onClick={() => dispatch(logout())}>
                            Logout
                        </Menu.Item>
                            :
                            <Menu.Item key="/login">
                                <Link to="/login">Login</Link>
                            </Menu.Item>}
                    </Col>

                </Header>
            </Row>
        </Menu >

    )
}