
import { useEffect} from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchTopic } from "../stores/topicSlice";
import { Topic } from "./Topic";

export function TopicList() {

    // const [post, setPost] = useState([])
    const dispatch = useDispatch();
    const topics = useSelector(state => state.topics.list)
    

    useEffect(() =>{
        dispatch(fetchTopic())
    }, [dispatch])

    

    // async function topicDel(id) {
    //     await AuthTopic.delete(id)
    //     setPost(
    //         post.filter(item => item.id !== id)
    //     );
    // }


    return(
        <div>
            {/* <Topic onDelete={topicDel} topic={post}/> */}
            {topics.map(item => <Topic /*onDelete={topicDel}*/ key={item.id} topic={item}/>)}
        </div>
    )
}