import React from 'react';
import { Upload, message, Button } from 'antd';
import { UploadOutlined } from '@ant-design/icons';


export function UploadImage({image}) {

    const props = {
        name: 'picture',
        action: image,
        headers: {
            authorization: 'authorization-text',
        },
        onChange(info) {
            if (info.file.status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (info.file.status === 'done') {
                message.success(`${info.file.name} file uploaded successfully`);
            } else if (info.file.status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
    };





    // const [fileList, setFileList] = useState([]);

    // const onChange = ({ fileList: newFileList }) => {
    //     setFileList(newFileList);
    // };

    // const onPreview = async file => {
    //     let src = file.url;
    //     if (!src) {
    //         src = await new Promise(resolve => {
    //             const reader = new FileReader();
    //             reader.readAsDataURL(file.originFileObj);
    //             reader.onload = () => resolve(reader.result);
    //         });
    //     }
    //     const image = new Image();
    //     image.src = src;
    //     const imgWindow = window.open(src);
    //     imgWindow.document.write(image.outerHTML);
    // };

    return (
        <Upload {...props}>
            <Button icon={<UploadOutlined />}>Click to Upload</Button>
        </Upload>


        // <ImgCrop>
        //     <Upload>
        //         <button>+ Add image</button>
        //     </Upload>
        // </ImgCrop>


        // <ImgCrop rotate>
        //     <Upload

        //         listType="picture-card"
        //         fileList={fileList}
        //         onChange={onChange}
        //         onPreview={onPreview}
        //     >
        //         {fileList.length < 5 && '+ Upload'}
        //     </Upload>
        // </ImgCrop>
    );
};