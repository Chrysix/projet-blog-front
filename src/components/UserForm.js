
import { Form, Input, Button } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { register } from '../stores/authSlice';

export function UserForm() {

    const dispatch = useDispatch()
    const feedback = useSelector(state => state.auth.registerFeedback)

    const onFinish = (values) => {
        dispatch(register(values));
        console.log('values' ,values);

    }

    return (
        <Form
            name="basic"
            labelCol={{
                span: 8,
            }}
            wrapperCol={{
                span: 10,
            }}
            onFinish={onFinish}
        >
            {feedback && <p>{feedback}</p>}

            <Form.Item
                label="First Name"
                name='first_name'
                type= 'text'
                rules={[
                    {
                        message: 'Please enter a valid First Name',
                    },
                ]}
            >
                
                <Input type="text" />
            </Form.Item>

            
            <Form.Item
                label="Name"
                name="name"
                rules={[
                    {
                        message: 'Please enter a valid Name',
                    },
                ]}
            >
                <Input type="text" />
            </Form.Item>
            
            <Form.Item
                label="Email"
                name="email"
                type="email"
                rules={[
                    {
                        required: true,
                        message: 'Email is required',
                    },

                ]}
            >
                <Input  />
            </Form.Item>

            <Form.Item
                label="Password"
                name="password"

                rules={[
                    {
                        required: true,
                        message: 'Password is required',
                    },
                ]}
            >
                <Input.Password />
            </Form.Item>



            <Form.Item
                wrapperCol={{
                    offset: 8,
                    span: 16,
                }}
            >
                <Button type="primary" htmlType="submit">
                    Register
                </Button>
            </Form.Item>
        </Form>
        
    )

}