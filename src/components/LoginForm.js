
import { Form, Input, Button } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect, Route } from 'react-router-dom';
import { log } from '../stores/authSlice';



export function LoginForm() {

    const user = useSelector(state => state.auth.user);
    const dispatch = useDispatch()
    const feedback = useSelector(state => state.auth.loginFeedback)

    const onFinish = (values) => {
        dispatch(log(values));
    }
    <Route>
        {user ? <Redirect to='/personal' /> : <Redirect to='/login' />
        }
    </Route>
    return (


        <Form
            name="basic"
            labelCol={{
                span: 8,
            }}
            wrapperCol={{
                span: 10,
            }}
            onFinish={onFinish}
        >
            {feedback && <p>{feedback}</p>}
            <Form.Item
                label="Email"
                name="email"
                rules={[
                    {
                        required: true,
                        message: 'Email is required',
                    },
                    {
                        type: 'email',
                        message: 'Please enter a valid email',
                    },
                ]}
            >
                <Input type="email" />
            </Form.Item>

            <Form.Item
                label="Password"
                name="password"
                rules={[
                    {
                        required: true,
                        message: 'Password is required',
                    },
                ]}
            >
                <Input.Password />
            </Form.Item>



            <Form.Item
                wrapperCol={{
                    offset: 8,
                    span: 16,
                }}
            >
                <Button type="primary" htmlType="submit">
                    Login
                </Button>
            </Form.Item>
        </Form>

    )
}