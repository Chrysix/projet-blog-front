
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Topic } from "../components/Topic";
import { fetchUserTopic } from "../stores/topicSlice";




export function PersonalPost() {
    const dispatch = useDispatch()
    const user = useSelector(state => state.auth.user);
    const post = useSelector(state => state.topics.userTopicList)


    useEffect(() => {
        if (user) {
            dispatch(fetchUserTopic(user.id));
        }
    }, [dispatch, user]);

    return (
        <div>
            {post.map(item => <Topic key={item.id} topic={item} />)}
        </div >
    )
}