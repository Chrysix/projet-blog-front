import { Button, Input, Form, Row, Col, Divider } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { UploadImage } from "../components/UploadImage";
import { postTopic } from "../stores/topicSlice";

export function TopicForm() {


    const feedback = useSelector(state => state.topics.postFeedBack)
    const topic = useSelector(state => state.topics.userTopicList);
    const dispatch = useDispatch()


    function handleSubmit(topic) {
        dispatch(postTopic(topic));

    }

    return (
        <>
            <Divider>
                <Form className='overflow' onFinish={handleSubmit}>
                    <Row>
                        <Col xs={8} xl={24}>
                            <UploadImage picture={topic.picture} />
                            <Form.Item label="Titre du post" name="title">
                                <Input type="text" />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={8} xl={24}>
                            <Form.Item label="Entrez votre message" name="message">
                                <Input.TextArea style={{ height: 200, width: 900 }} />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={8} xl={24}>
                            <Button type="primary" htmlType="submit">Submit</Button>
                            {feedback && <p>{feedback}</p>}
                        </Col>
                    </Row>
                </Form>
            </Divider>

        </>
    )
}