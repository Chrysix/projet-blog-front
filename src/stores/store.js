import { configureStore } from "@reduxjs/toolkit";
import authSlice from "./authSlice";
import topicSlice from "./topicSlice";
import userSlice from "./userSlice";


export const store = configureStore({
    reducer: {
        auth: authSlice,
        users: userSlice,
        topics: topicSlice
    }
});