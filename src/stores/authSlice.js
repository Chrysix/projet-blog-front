import { createSlice } from "@reduxjs/toolkit";
import { AuthService } from "../services/AuthService";
import {addUser} from "./userSlice";


const authSlice = createSlice({
    name: 'auth',
    initialState: {
        user: null,
        loginFeedback: '',
        registerFeedback: '',
    },
    reducers: {
        login(state, { payload }) {
            state.user = payload;
        },

        logout(state) {
            state.user = null;
            localStorage.removeItem('token')
        },

        updateLoginFeedback(state, { payload }) {
            state.loginFeedback = payload
        },

        updateRegisterFeedback(state, { payload }) {
            state.registerFeedback = payload
        }
    }
})

export const { login, logout } = authSlice.actions;

export default authSlice.reducer;

export const loginWithToken = () => async (dispatch) => {
    const token = localStorage.getItem('token')

    if (token) {
        try {
            const user = await AuthService.fetchAccount();
            dispatch(login(user));
        } catch (error) {
            dispatch(login());
        }

    }

}

export const { updateRegisterFeedback, updateLoginFeedback } = authSlice.actions;

export const register = (regist) => async (dispatch) => {
    try {
        const user = await AuthService.register(regist);
        dispatch(updateRegisterFeedback('Success'))
        dispatch(addUser(user))
        
    } catch (error) {
        dispatch(updateRegisterFeedback(error.response.data.error));
    }
}

export const log = (credentials) => async (dispatch) => {
    try {
        const user = await AuthService.login(credentials);
        dispatch(updateLoginFeedback('Success'))
        dispatch(login(user))
    } catch (error) {
        dispatch(updateLoginFeedback('error'));
    }
}
