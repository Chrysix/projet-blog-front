import { createSlice } from "@reduxjs/toolkit";
import { AuthTopic } from "../services/AuthTopic";

export const topicSlice = createSlice({
    name: 'topics',
    initialState: {
        id: null,
        list: [],
        userTopicList: [],
        postFeedBack:''
    },

    reducers: {
        setOneTopic(state, { payload }){
            state.id = payload
        },

        setTopic(state, { payload }) {
            state.list = payload
        },

        addTopic(state, {payload}){
            state.list.push(payload)
        },

        addPersonalTopic(state, {payload}){
            state.userTopicList.push(payload)
        },

        updatePostFeedBack(state, { payload }) {
            state.postFeedBack = payload
        },

        // deleteTopic(state, {payload}){ //A modifié car pas bon
        //     state.list = payload
        // },

        setUserTopic(state, { payload }) {
            state.userTopicList = payload
        }
    }
})

export const { addTopic, setTopic, setUserTopic, deleteTopic, updatePostFeedBack, addPersonalTopic, setOneTopic } = topicSlice.actions

export default topicSlice.reducer


export const fetchTopic = () => async (dispatch) => {
    try {
        const response = await AuthTopic.fetchAll();
        dispatch(setTopic(response))

        console.log(response.data)
    } catch (error) {
        console.log(error);
    }
}

export const postTopic = (topic) => async (dispatch) => {
    try {
        const response = await AuthTopic.add(topic);
        dispatch(updatePostFeedBack('Message Send'))
        dispatch(addTopic(response))
        dispatch(addPersonalTopic(response))

    } catch (error) {
        console.log(error);
        dispatch(updatePostFeedBack('Error'))
    }
}

export const topicDelete = (id) => async (dispatch) => {
    try {
        const response = await AuthTopic.delete(id);
        dispatch(deleteTopic(response))
        console.log(response);
    } catch (error) {
        console.log(error);
    }
}

export const fetchUserTopic = (id) => async (dispatch) => {/*, getState*/
    try {
        const response = await AuthTopic.findbyUser(id);/*getState().auth.user.id*/
        dispatch(setUserTopic(response))
    } catch (error) {
        console.log(error);
    }
}

export const fetchTopicId = (id) => async (dispatch) => {
    try {
        const response = await AuthTopic.findbyId(id);
        dispatch(setOneTopic(response))
    } catch (error) {
        console.log(error);
    }
}