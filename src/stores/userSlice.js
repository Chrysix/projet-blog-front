import { createSlice } from "@reduxjs/toolkit";
import { AuthService } from "../services/AuthService";


export const userSlice = createSlice({
    name: 'users',
    initialState:{
        list: []
    },
    reducers:{
        setList(state, { payload }){
            state.list = payload;
        },

        addUser(state, { payload }){
            state.list.push(payload)
        }
    }
})

export const {addUser, setList} = userSlice.actions

export default userSlice.reducer

export const fetchUsers = (user) => async (dispatch) => {
    try {
        const response = await AuthService.fetchAll(user);
        dispatch(setList(response.data))

    } catch (error) {
        console.log(error);
    }
}

export const fetchOneUser = (id) => async (dispatch) => {
    try {
        const response = await AuthService.fetchAccount(id);
        dispatch(setList(response))

    } catch (error) {
        console.log(error);
    }
}

