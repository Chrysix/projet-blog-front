import { Layout } from "antd";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Redirect, Route, Switch } from "react-router-dom";
import { Nav } from "./components/Nav";
import { ProtectedRoute } from "./components/ProtectedRoute";
import { Home } from "./pages/Home";
import { Login } from "./pages/Login";
import { Personal } from "./pages/Personal";
import { Register } from "./pages/Register";
import { TopicOne } from "./pages/TopicOne";
import { loginWithToken } from "./stores/authSlice";

const { Header, Content } = Layout;


function App({ children, ...rest }) {
    const dispatch = useDispatch();
    const user = useSelector(state => state.auth.user);

    useEffect(() => {
        dispatch(loginWithToken());
    }, [dispatch]);

    return (
        <div>
            <Layout>
                <Header className='nav'>
                    <Nav />
                </Header>
                <Content>

                    <Switch>
                        <Route path="/" exact>
                            <Home />
                        </Route>


                        <Route path="/login" {...rest}>
                            <Login />
                            {user ? <Redirect to='/personal' /> : children}
                        </Route>

                        <Route path="/register">
                            <Register />
                        </Route>

                        <ProtectedRoute path="/personal">
                            <Personal />
                        </ProtectedRoute>

                        <Route path="/topic/:id">
                            <TopicOne />

                        </Route>

                    </Switch>
                </Content>
            </Layout>
        </div >
    );
}

export default App;
