import axios from "axios";



export class AuthService {


    // static async register(regist) {
    //     const response = await axios.post('http://localhost:8000/api/user', regist);
    //     return response.data;
    // }

    static async register(regist) {
        return axios.post(process.env.REACT_APP_SERVER_URL+ '/api/user', regist);
        
    }


    // static async login(credentials) {
    //     const response = await axios.post('http://localhost:8000/api/user/login', credentials);
        
    //     localStorage.setItem('token', response.data.token);
        
    //     return response.data.user;
    // }

    static async login(credentials) {
        return axios.post(process.env.REACT_APP_SERVER_URL+ '/api/user/login', credentials);
    }

    // static async fetchAccount() {
    //     const response = await axios.get('http://localhost:8000/api/user/account/');
        
    //     return response.data;
    // }

    static async fetchAccount() {
        return axios.get(process.env.REACT_APP_SERVER_URL+ '/api/user/account/');
    }

    // static async fetchAll() {
    //     const response = await axios.get('http://localhost:8000/api/user');
        
    //     return response.data;
    // }

    static async fetchAll() {
        return axios.get(process.env.REACT_APP_SERVER_URL+ '/api/user');
    }

}