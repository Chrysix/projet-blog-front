import axios from "axios";



export class AuthTopic {


    // static async add(topic) {
    //     const response = await axios.post('http://localhost:8000/api/topic/', topic);

    //     return response.data;
    // }

    static async add(topic) {
        return axios.post(process.env.REACT_APP_SERVER_URL+ '/api/topic/', topic);
    }


    // static async delete(id) {
    //     const response = await axios.post('http://localhost:8000/api/topic/', id);
        
    //     return response.data.user;
    // }

    static async delete(id) {
        return axios.post(process.env.REACT_APP_SERVER_URL+ '/api/topic/', id);
    }

    // static async update(modif) {
    //     const response = await axios.patch('http://localhost:8000/api/topic/', modif);
        
    //     return response.data;
    // }

    static async update(modif) {
        return axios.patch(process.env.REACT_APP_SERVER_URL+ '/api/topic/', modif);
    }

    // static async findbyId(id) {
    //     const response = await axios.get('http://localhost:8000/api/topic/'+ id);
        
    //     return response.data;
    // }

    static async findbyId(id) {
        return axios.get(process.env.REACT_APP_SERVER_URL+ '/api/topic/', id);
    }

    // static async findbyUser(id) {
    //     const response = await axios.get('http://localhost:8000/api/topic/user/'+ id);
        
    //     return response.data;
    // }

    static async findbyUser(id) {
        return axios.get(process.env.REACT_APP_SERVER_URL+ '/api/topic/user/', id);
    }

    // static async fetchAll() {
    //     const response = await axios.get('http://localhost:8000/api/topic');
        
    //     return response.data;
    // }

    static async fetchAll() {
        return axios.get(process.env.REACT_APP_SERVER_URL+ '/api/topic');
    }

}