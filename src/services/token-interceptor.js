import axios from "axios";

axios.interceptors.request.use((config) => {
    const token =localStorage.getItem('token');
    if(token) {

        config.headers.authorization = 'bearer '+token;
    }
    return config;
});

// axios.interceptors.response.use(function (response) {
//     return response;
// }, function (error) {
//     if (401 === error.response.status) {
//         swal({
//             title: "Session Expired",
//             text: "Your session has expired. Would you like to be redirected to the login page?",
//             type: "warning",
//             showCancelButton: true,
//             confirmButtonColor: "#DD6B55",
//             confirmButtonText: "Yes",
//             closeOnConfirm: false
//         }, function(){
//             window.location = '/login';
//         });
//     } else {
//         return Promise.reject(error);
//     }
// });