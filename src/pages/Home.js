import { Col, Divider, Row } from "antd";
import { useSelector } from "react-redux";
import { TopicForm } from "../components/TopicForm";
import { TopicList } from "../components/TopicList";


export function Home() {

    // const topics = useSelector(state => state.topics.userTopicList)

    const user = useSelector(state => state.auth.user);



    // useEffect(() => {
    //     AuthService.fetchAccount().then(data => console.log(data));
    // }, [])

    return (
        <div>
            <Row>
                <Col span={24}>
                    <Divider>
                        <h1>Home</h1>
                    </Divider>
                </Col>
            </Row>

            <Row>
                <Col span={24}>
                    <Divider>
                        {user ? 
                        <TopicForm /> 
                        : "Connecter vous pour pouvoir faire un Post"}
                        <TopicList />
                    </Divider>
                    {/* <Menu /> */}
                </Col>
            </Row>
        </div >





    )
}