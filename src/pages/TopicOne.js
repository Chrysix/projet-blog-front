
import { Topic } from "../components/Topic";

import { useEffect} from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchTopicId } from "../stores/topicSlice";

export function TopicOne() {

    const topics = useSelector(state => state.topics.id)
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchTopicId())
    }, [dispatch])


    return (
        <div>
            <p>Coucou</p>
            <Topic topic={topics.id} />
            <Topic topic={topics.message}/>
            <p ></p>
        </div>
    )
}

/*{topics.map(item => <Topic onDelete={topicDel} key={item.id} topic={item} />)}*/