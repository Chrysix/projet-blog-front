
import { Col, Divider, Row } from "antd";
import { useSelector } from "react-redux";
import { PersonalPost } from "../components/PersonalPost";
import { TopicForm } from "../components/TopicForm";




export function Personal() {
    const user = useSelector(state => state.auth.user);

    return (
        <div>
            <Row>
                <Col span={24}>
                    <Divider>
                        <h1>Ma page</h1>
                        Bienvenue sur votre page personel, {user && user.first_name}
                    </Divider>
                </Col>
            </Row>
            <Row>
                <Col span={24}>
                    <Divider>
                        <TopicForm />
                    </Divider>
                    <PersonalPost />
                </Col>
            </Row>
            {/* <Menu /> */}
        </div >
    )
}